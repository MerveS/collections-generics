public class App {

    public static void main(String[] args) {

        Store store = new Store();
        //store.setItem("Fındık");
        store.setItem(45);
       // System.out.println(store);

        //we dont know what type of object returns back
        //cast has to (Integer)
       // Integer item =  store.getItem();

        //Java checks StoreGeneric class, and ok its generic, and then puts String instead of T
        StoreGeneric<String> stringStoreGeneric = new StoreGeneric<>();
        stringStoreGeneric.setItem("setItem takes automatically String values");
        //dont have to cast, Java knows what type returns back
      /*  String item = stringStoreGeneric.getItem();
        System.out.println(item);*/

        StoreGeneric<Integer> integerStoreGeneric = new StoreGeneric<>();
        integerStoreGeneric.setItem(5);
        //don't have to cast, Java knows what type returns back
        //it checks at compile time
        Integer integerItem = integerStoreGeneric.getItem();

        Store withException = new Store();
        withException.setItem(5);
        //getItem2 returns String, there is no compile time error, but there is runtime error
        //Integer integerItem2 = (Integer) withException.getItem2();

    }
}
