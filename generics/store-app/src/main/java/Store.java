public class Store {

    //we dont know what is going to be stored
    private Object item;

    public Object getItem() {
        return item;
    }

    public Object getItem2() {
        return "abc";
    }

    public void setItem(Object item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return this.item.toString();
    }
}
